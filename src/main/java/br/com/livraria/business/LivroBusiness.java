package br.com.livraria.business;

import java.util.List;
import java.util.Objects;
import br.com.livraria.dao.LivroDAO;
import br.com.livraria.entidade.Livro;

	public class LivroBusiness {

		LivroDAO dao = new LivroDAO();
		
		public void criarLivro( Livro livro) {
			if ( Objects.isNull(livro)) {
				throw new NullPointerException("Registro nulo!");
			}
			this.dao.criarLivro(livro);
		}
		
		public Livro buscaLivroPorId(Integer id) {
			return this.dao.buscaLivroPorId(id);
		}
		
		public List<Livro> buscaLivroPorAutor(Integer id) {
			return this.dao.buscaLivroPorAutor(id);
		}
}
