package br.com.livraria.business;

import java.util.Objects;

import br.com.livraria.dao.AutorDAO;
import br.com.livraria.entidade.Autor;

public class AutorBusiness {
	
	AutorDAO dao = new AutorDAO();

	public void criarAutor(Autor autor) {
		if ( Objects.isNull(autor)) {
			throw new NullPointerException("Registro nulo!");
		}
		this.dao.criarAutor(autor);
	}
	
}
