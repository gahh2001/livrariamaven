package br.com.livraria.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Autor {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id_autor;
	
	@Column
	private String nome;
	
	@Column
	private String dtNascimento;
	
	@Column
	private String localNascimento;
	
	public Autor( String nome, String dtNascimento, String localNascimento) {
		super();
		this.nome = nome;
		this.dtNascimento = dtNascimento;
		this.localNascimento = localNascimento;
	}
	

	@Override
	public String toString() {
		return "Autor =" + nome;
	}

	public Autor() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getLocalNascimento() {
		return localNascimento;
	}

	public void setLocalNascimento(String localNascimento) {
		this.localNascimento = localNascimento;
	}
	
}
