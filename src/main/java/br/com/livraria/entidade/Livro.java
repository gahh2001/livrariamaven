package br.com.livraria.entidade;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table( name = "livro")
public class Livro {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id_livro;
	
	@Column(nullable = false)
    private String titulo;

    @Column
    private String tituloOriginal;

    @Column
    private String editora;

    @Column
    private int noPaginas;

    @Column
    private int noEdicao;

    @ManyToMany
    @JoinTable(name= "autor_livro", joinColumns= @JoinColumn(name= "id_livro"), 
    inverseJoinColumns = @JoinColumn(name= "id_autor"))
    private List<Autor> autores;
    
    public List<Autor> getAutor() {
		return autores;
	}

	public void setAutor(List<Autor> autor) {
		this.autores = autor;
	}

	public Livro() {
		super();
	}

	public Livro(String titulo, String tituloOriginal, String editora, int noPaginas, int noEdicao,
			Autor autor) {
		super();
		this.titulo = titulo;
		this.tituloOriginal = tituloOriginal;
		this.editora = editora;
		this.noPaginas = noPaginas;
		this.noEdicao = noEdicao;
	}

	@Override
	public String toString() {
		return "Livro [id=" + id_livro + ", titulo=" + titulo + ", tituloOriginal=" + tituloOriginal + ", editora=" + editora
				+ ", noPaginas=" + noPaginas + ", noEdicao=" + noEdicao + "]";
	}

	public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTituloOriginal() {
        return tituloOriginal;
    }

    public void setTituloOriginal(String tituloOriginal) {
        this.tituloOriginal = tituloOriginal;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getNoPaginas() {
        return noPaginas;
    }

    public void setNoPaginas(int noPaginas) {
        this.noPaginas = noPaginas;
    }

    public int getNoEdicao() {
        return noEdicao;
    }

    public void setNoEdicao(int noEdicao) {
        this.noEdicao = noEdicao;
    }

}
