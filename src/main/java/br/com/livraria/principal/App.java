package br.com.livraria.principal;

import java.util.ArrayList;
import java.util.List;

import br.com.livraria.business.AutorBusiness;
import br.com.livraria.business.LivroBusiness;
import br.com.livraria.entidade.Autor;
import br.com.livraria.entidade.Livro;

public class App {

	public static void main(String[] args) {
				
		LivroBusiness livroBusiness = new LivroBusiness();
		AutorBusiness autorBusiness = new AutorBusiness();
		
		List<Livro> listaLivros1 = new ArrayList<>();
		List<Livro> listaLivros2 = new ArrayList<>();
		
		List<Autor> listaAutores1 = new ArrayList<>();
		
		Autor autor1 = new Autor("Gabriel", "06/03/2001", "RS");	
		Autor autor2 = new Autor("Helio", "01/03/2001", "SC");	
		Autor autor3 = new Autor("Louise", "15/03/2001", "AM");	
		
		Livro livro1 = new Livro("Joao e Maria", "John and Maria", "FolhaBoa", 200, 84521, autor1);
		Livro livro2 = new Livro("As aventuras de Pi", "Adventure's Pi", "FolhaBoa", 300, 98514, autor2);
		Livro livro3 = new Livro("Peter Pan", "Peter Pan", "Livrus", 100, 97054, autor3);
		Livro livro4 = new Livro("Cinderela", "Cindy", "Disney", 240, 98520, autor1);
		Livro livro5 = new Livro("JiCars", "Carros exóticos", "CityWord", 50, 97382, autor2);
		
		listaLivros1.add(livro1);
		listaLivros1.add(livro2);
		listaLivros2.add(livro3);
		listaLivros2.add(livro4);
		listaLivros2.add(livro5);
		
		livro1.setAutor(listaAutores1);
		livro2.setAutor(listaAutores1);
		livro3.setAutor(listaAutores1);
		livro4.setAutor(listaAutores1);
		livro5.setAutor(listaAutores1);
	
		autorBusiness.criarAutor(autor1);
		autorBusiness.criarAutor(autor2);
		autorBusiness.criarAutor(autor3);
		
		livroBusiness.criarLivro(livro1);
		livroBusiness.criarLivro(livro2);
		livroBusiness.criarLivro(livro3);
		livroBusiness.criarLivro(livro4);
		livroBusiness.criarLivro(livro5);
		
		System.out.println(livroBusiness.buscaLivroPorId(84521));
		System.out.println(livroBusiness.buscaLivroPorId(97054));
		System.out.println(livroBusiness.buscaLivroPorId(97382));
		System.out.println(livroBusiness.buscaLivroPorAutor(1));
		System.out.println(livroBusiness.buscaLivroPorAutor(2));

	}

}