package br.com.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.livraria.entidade.Livro;

public class LivroDAO {

    public void criarLivro(Livro livro) {
    	EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("livraria");
    	EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(livro);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }
    
    public Livro buscaLivroPorId(Integer id) {
    	EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("livraria");
    	EntityManager entityManager = entityManagerFactory.createEntityManager();
    	
        entityManager.getTransaction().begin();
        Livro livro = entityManager.find(Livro.class , id);
        
        entityManager.close();
        entityManagerFactory.close();
        return livro; 
    }
    
    public List<Livro> buscaLivroPorAutor( Integer id ) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("livraria");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        
        entityManager.getTransaction().begin();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery< Livro > query = builder.createQuery( Livro.class );
        Root<Livro> from = query.from(Livro.class);
        query.where(builder.equal(from.get("id_livro"), id));
        List<Livro> livros = entityManager.createQuery(query).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return livros;
    }
    
}